local HTTPutils = require 'HTTP.HTTPutils'

local txUrl = function (a, v)
   local splitParamsInUrl = function (t)
      local temp = {}
      t = t:split('?')[2]
      if t then
         t = t:split('&')

         for _, v in pairs(t) do
            local t = v:split('=')
            temp[t[1]] = t[2]
         end
      end
      return temp
   end

   local temp = HTTPutils.HTTPurlParser(v)
   a.parameters = splitParamsInUrl(temp.endpoint)
   temp.endpoint = temp.endpoint:split('?')[1]
   return temp, a.parameters
end

local addMoreHeaders = function (t, m, n)
   -- RFC 2616 https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
   local temp = n:split(':')
   if t[temp[1]] then
      t[temp[1]:trimWS()] = t[temp[1]:trimWS()]..','..temp[2]:trimWS()
   else
      t[temp[1]:trimWS()] = temp[2]:trimWS()
   end
   return t
end

local txHeaders = function (t, v)
   if type(v) == 'table' then

      for m,n in pairs(v) do
         if type(m) == 'number' and type(n) == 'string' then
            t =  addMoreHeaders(t, m, n)
         else
            return v -- no changes to Headers table
         end
      end
   end
   return t
end

local checkPermission = function (methods, permitted)
   local temp = ''
   for k, v in pairs(methods) do
      if not permitted[v] then
         methods[k] = nil
      end
   end
   return methods
end

local txAuth = function (t, v)
   local permittedAuthMethods            = {}
   permittedAuthMethods.basic            = true
   permittedAuthMethods.digest           = true
   permittedAuthMethods['digest-ie']     = true
   permittedAuthMethods['gss-negotiate'] = true
   permittedAuthMethods.ntlm             = true
   permittedAuthMethods.any              = true
   permittedAuthMethods.anysafe          = true

   t.method = checkPermission(v.method, permittedAuthMethods)

   if v.password and type(v.password) == 'string' then
      t.password = v.password:trimWS()
   end

   if v.username and type(v.username) == 'string' then
      t.username = v.username:trimWS()
   end

   if v.follow then
      t.follow = v.follow
   end
   return t
end

local txSSL = function (t, w)
   local permittedSSLversions            = {}
   permittedSSLversions['ssl-v3']        = true
   permittedSSLversions['tls-v1']        = true
   permittedSSLversions['tls-v1.1']      = true
   permittedSSLversions['tls-v1.2']      = true

   local keyType                         = {}
   keyType.PEM                           = true
   keyType.DER                           = true
   keyType.ENG                           = true

   local certType                        = {}
   certType.PEM                          = true
   certType.DER                          = true

   local sslStringTypeArgs               = {}
   sslStringTypeArgs.cert                = true
   sslStringTypeArgs.cert_type           = true
   sslStringTypeArgs.key                 = true
   sslStringTypeArgs.key_pass            = true
   sslStringTypeArgs.key_type            = true
   sslStringTypeArgs.ssl_engine          = true
   sslStringTypeArgs.ca_file             = true
   sslStringTypeArgs.issuer_cert         = true
   sslStringTypeArgs.crl_file            = true

   local sslBooleanTypeArgs              = {}
   sslBooleanTypeArgs.verify_peer        = true
   sslBooleanTypeArgs.verify_host        = true

   local validSSLversions = function (t)

      for k, v in pairs (t) do
         if not permittedSSLversions[v] then
            t[k] = nil
         end
      end
      return t
   end

   for k, v in pairs (w) do
      if sslStringTypeArgs[k] and type(v) ~= 'string' then
         w[k] = nil
      else
         t[k] = v
      end

      if sslBooleanTypeArgs[k] and type(v) ~= 'boolean' then
         w[k] = nil
      else
         t[k] = v
      end

      if k == 'cert_type' and not certType[v:upper()] then
         t[k] = nil
      end

      if k == 'key_type' and not keyType[v:upper()] then
         t[k] = nil
      end

      if k == 'ssl_version' then
         t[k] = validSSLversions(t[k])
      end
   end

   if t.key_type ~= 'ENG' then
      t.ssl_engine = nil
   end
   return t
end

local selector = function (a, args)
   if type(args.url) == 'string' then
      a.urlT, a.parameters = txUrl(a, args.url)
   end

   for k,v in pairs(args) do
      if k == 'timeout' and type(v) == 'number' then
         a.timeout = v
      end

      if k == 'parameters' and type(v) == 'table' then
         a.parameters = HTTPutils.join2LuaTables(a.parameters, v)
      end

      if a.method == 'POST' and
         (k == 'get_parameters' and type(v) == 'table') then
         a.get_parameters = v
      end

      if k == 'headers' then
         a.headers = txHeaders(a.headers, v)
      end

      if k == 'response_headers_format' and type(v) == 'string' then
         if v == 'default' or v == 'raw' or v == 'multiple' then
            a.response_headers_format = v:trimWS()
         end
      end

      if a.method == 'POST' and
         (k == 'body' and type(v) == 'string') then
         a.body = v:trimWS()
      end

      if k == 'auth' and type(v) == 'table' then
         a.auth = txAuth(a.auth, v)
      end

      if k == 'ssl' and type(v) == 'table' then
         a.ssl = txSSL(a.ssl, v)
      end

      if k == 'method' and type(v) == 'string' then
         a.method = v:trimWS()
      end

      if k == 'cache_connection_by_host ' and type(v) == 'boolean' and v then
         a.cache_connection_by_host = true
      end

      if k == 'debug' and type(v) == 'boolean' and v then
         a.debug = true
      end

      if k == 'live' and type(v) == 'boolean' and v then
         a.live = true
      end
   end
   return a
end

local defaults = function ()
   local a                            = {}
   a.url                              = ''
   a.urlT                             = {}
   a.timeout                          = 10 -- 10 is the default in line 187 of LSCKsocket.cpp
   a.parameters                       = {}
   a.get_parameters                   = {}
   a.headers                          = {}
   a.response_headers_format          = "default"
   a.method                           = ""
   a.body                             = nil
   a.auth                             = {}
   a.ssl                              = {}
   a.ssl.cert_type                    = 'PEM'
   a.ssl.key_type                     = 'PEM'
   a.ssl.verify_peer                  = true
   a.ssl.verify_host                  = true
   a.cache_connection_by_host         = false
   a.debug                            = false
   a.live                             = false
   return a
end

local txPostArgs = function (args)
   local a  = defaults()
   a.method = 'POST'
   return selector (a, args)
end

local txGetArgs = function (args)
   local a  = defaults()
   a.method = 'GET'
   return selector (a, args)
end

return {
   txPostArgs = txPostArgs,
   txGetArgs  = txGetArgs
}
